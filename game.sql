-- Database: Game

-- DROP DATABASE "Game";

CREATE DATABASE "Game"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Ukraine.1251'
    LC_CTYPE = 'Russian_Ukraine.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
CREATE EXTENSION pgcrypto;
SELECT gen_random_uuid();

DROP TABLE gamer CASCADE;
DROP TABLE character CASCADE;
DROP TABLE hard_level CASCADE;
DROP TABLE monster_type CASCADE;
DROP TABLE monster CASCADE;
DROP TABLE arena CASCADE;
DROP TABLE arena_monster CASCADE;

-- CREATE TABLES --
CREATE TABLE gamer(
	id varchar(50) PRIMARY KEY DEFAULT gen_random_uuid(),
	gamer_nick varchar(50) NOT NULL
);
CREATE TABLE character(
	id varchar(50) PRIMARY KEY DEFAULT gen_random_uuid(),
	character_name varchar(50) NOT NULL UNIQUE,
	lvl int NOT NULL DEFAULT 0,
	skill_percent int NOT NULL DEFAULT 0,
	id_gamer varchar(50) REFERENCES gamer(id)
);
CREATE TABLE hard_level(
	title varchar(50) PRIMARY KEY UNIQUE NOT NULL
);
CREATE TABLE monster_type(
	id varchar(50) PRIMARY KEY DEFAULT gen_random_uuid(),
	type varchar(50) UNIQUE NOT NULL
);
CREATE TABLE monster(
	id varchar(50) PRIMARY KEY DEFAULT gen_random_uuid(),
	hard_level varchar(50) REFERENCES hard_level(title),
	id_monster_type varchar(50) REFERENCES monster_type(id)
);
CREATE TABLE arena(
	id varchar(50) PRIMARY KEY DEFAULT gen_random_uuid(),
	name varchar(50) NOT NULL,
	UNIQUE(name)
);
CREATE TABLE arena_monster(
	id_arena varchar(50) NOT NULL,
	id_monster varchar(50) NOT NULL,
	FOREIGN KEY (id_arena) REFERENCES arena(id),
	FOREIGN KEY (id_monster) REFERENCES monster(id)
);

-- INSERT GAMERS --
INSERT INTO gamer(gamer_nick) VALUES('Gamer1'),
									('Gamer2');
-- CHECK GAMERS --
-- SELECT * FROM gamer;

-- STORED PROCEDURE FOR INSERT CHARACTERS BY NAME --
CREATE OR REPLACE FUNCTION get_id_by_gamer_nick(_gamer_nick varchar(50)) RETURNS varchar(50)
	AS $$ SELECT id FROM gamer WHERE gamer_nick = _gamer_nick $$
	LANGUAGE SQL;

CREATE OR REPLACE PROCEDURE insert_character(_character_name varchar(50),_lvl int,_skill_percent int, _gamer_nick varchar(50))
LANGUAGE SQL
AS $$
	INSERT INTO character(character_name, lvl, skill_percent, id_gamer) 
		VALUES(_character_name, _lvl, _skill_percent, get_id_by_gamer_nick(_gamer_nick));
$$;

-- INSERT CHARACTERS WITH STORED PROCEDURE --
CALL insert_character('Triton', 25, 89, 'Gamer1');
CALL insert_character('Maribella', 11, 43, 'Gamer1');
CALL insert_character('Patron', 74, 16, 'Gamer2');
CALL insert_character('Gitara', 96, 55, 'Gamer2');
-- CHECK CHARACTERS --
-- SELECT * FROM character;

-- INSERT LEVEL DIFFICULTY --
INSERT INTO hard_level(title) VALUES('easy'),('medium'),('hard'),('unreal');
-- CHECK LEVEL DIFFICULTY --
-- SELECT * FROM hard_level;

-- INSERT MONSTERS TYPES
INSERT INTO monster_type(type) VALUES('ork'),('goblin'),('witch'),('ghost');
-- CHECK MONSTER TYPES --
-- SELECT * FROM monster_type;

-- STORED PROCEDURE FOR INSERT MONSTER BY TYPE
CREATE OR REPLACE FUNCTION get_id_by_monster_type(_monster_type varchar(50)) RETURNS varchar(50)
	AS $$ SELECT id FROM monster_type WHERE type = _monster_type $$
	LANGUAGE SQL;

CREATE OR REPLACE PROCEDURE insert_monster(hard_level_arg varchar(50), monster_type_arg varchar(50))
LANGUAGE SQL
AS $$
	INSERT INTO monster(hard_level, id_monster_type) 
		VALUES(hard_level_arg, get_id_by_arg(monster_type_arg));
$$;

-- INSERT MONSTERS WITH STORED PROCEDURE
CALL insert_monster('easy', 'goblin');
CALL insert_monster('hard', 'goblin');
CALL insert_monster('medium', 'goblin');
CALL insert_monster('unreal', 'goblin');
CALL insert_monster('easy', 'ork');
CALL insert_monster('hard', 'ork');
CALL insert_monster('medium', 'ork');
CALL insert_monster('unreal', 'ork');
CALL insert_monster('easy', 'witch');
CALL insert_monster('hard', 'witch');
CALL insert_monster('medium', 'witch');
CALL insert_monster('unreal', 'witch');
CALL insert_monster('easy', 'ghost');
CALL insert_monster('hard', 'ghost');
CALL insert_monster('medium', 'ghost');
CALL insert_monster('unreal', 'ghost');
-- CHECK MONSTERS --
-- SELECT * FROM monster;

-- INSERT ARENAS --
INSERT INTO arena(name) VALUES('Night wood'), ('Mistery'), ('Big world');
-- CHECK ARENAS --
-- SELECT * FROM arena;

-- STORED PROCEDURE FOR INSERT MONSTERS TO ARANAS --
CREATE OR REPLACE FUNCTION get_id_by_arena_name(_arena_name varchar(50)) RETURNS varchar(50)
	AS $$ SELECT id FROM arena WHERE name = _arena_name $$
	LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_id_by_monster_hard_level_and_type(_monster_type varchar(50), _monster_hard_level varchar(50))
	RETURNS varchar(50)
	AS $$ SELECT id FROM monster WHERE id_monster_type = get_id_by_monster_type(_monster_type) AND hard_level = _monster_hard_level $$
	LANGUAGE SQL;

CREATE OR REPLACE PROCEDURE insert_monster_to_arena(_arena_name varchar(50),_monster_type varchar(50), _monster_hard_level varchar(50))
LANGUAGE SQL
AS $$
	INSERT INTO arena_monster(id_arena, id_monster) VALUES(get_id_by_arena_name(_arena_name), get_id_by_monster_hard_level_and_type(_monster_type, _monster_hard_level));
$$;

-- INSERT CHARACTERS WITH STORED PROCEDURE --
CALL insert_monster_to_arena('Mistery', 'ork', 'easy');
CALL insert_monster_to_arena('Mistery', 'ork', 'hard');
CALL insert_monster_to_arena('Mistery', 'ghost', 'easy');
CALL insert_monster_to_arena('Mistery', 'ghost', 'easy');
CALL insert_monster_to_arena('Mistery', 'ghost', 'hard');
CALL insert_monster_to_arena('Mistery', 'ghost', 'unreal');
CALL insert_monster_to_arena('Mistery', 'witch', 'easy');

CALL insert_monster_to_arena('Big world', 'ork', 'easy');
CALL insert_monster_to_arena('Big world', 'ork', 'hard');
CALL insert_monster_to_arena('Big world', 'ghost', 'easy');
CALL insert_monster_to_arena('Big world', 'ghost', 'easy');
CALL insert_monster_to_arena('Big world', 'ghost', 'hard');
CALL insert_monster_to_arena('Big world', 'ghost', 'unreal');
CALL insert_monster_to_arena('Big world', 'witch', 'easy');
-- CHECK CHARACTERS --
-- SELECT * FROM arena_monster;

-- SHOW MONSTERS AND ARENAS --
SELECT arena.name, monster.hard_level, monster_type.type FROM arena JOIN arena_monster
	ON arena.id = arena_monster.id_arena JOIN monster ON arena_monster.id_monster = monster.id
		JOIN monster_type ON monster.id_monster_type = monster_type.id;

-- SHOW GAMERS AND THEIR CHARACTERS --
SELECT gamer.gamer_nick, character.character_name FROM character 
	JOIN gamer ON character.id_gamer = gamer.id;
 
